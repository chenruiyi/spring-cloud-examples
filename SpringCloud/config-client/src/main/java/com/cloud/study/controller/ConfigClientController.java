package com.cloud.study.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope   //用于刷新配置，POST方式请求/refresh
public class ConfigClientController {

    @Value("${profile}")
    private String profile;
    
    @RequestMapping("/profile")
    public String profile(){
        return this.profile;
    }
}
