package com.cloud.study.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServiceAController {
    
    @RequestMapping("/add")
    public Integer add(Integer a,Integer b){
        return a+b;
    }
}
