package com.cloud.study.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class ConfigClientEurekaController {

    @Value("${profile}")
    private String profile;
    
    @RequestMapping("/eureka/profile")
    public String profile(){
        return this.profile;
    }
}
