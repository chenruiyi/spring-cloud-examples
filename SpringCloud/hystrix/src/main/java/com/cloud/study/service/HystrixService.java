package com.cloud.study.service;

import com.cloud.study.model.User;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class HystrixService {
    
    @Autowired
    private RestTemplate restTemplate;
    
    @HystrixCommand(fallbackMethod = "fallback")
    public String add(Integer a,Integer b){
        String url="http://spring-cloud-service/add?a={a}&b={b}";
        if(a != null && b != null){
            url="http://spring-cloud-service/add?a="+a+"&b="+b;
        }
        return restTemplate.getForEntity(url, String.class,99,-18).getBody();
    }
    
    public String fallback(Integer a,Integer b){
        User user=new User();
        user.setId(-1);
        user.setName("张三a");
        user.setAge(36);
        return user.toString();
    }
}
