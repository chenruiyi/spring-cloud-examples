package com.cloud.study.controller;

import com.cloud.study.service.HystrixService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HystrixController {
    
    @Autowired
    private HystrixService hystrixService;
    
    @RequestMapping("/add")
    public String add(Integer a,Integer b){
        return hystrixService.add(a,b);
    }
}
