package com.cloud.study.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class ServiceBController {
    
    private RestTemplate restTemplate=new RestTemplate();
    
    @RequestMapping("/add")
    public Integer add(Integer a,Integer b){
        return a+b;
    }
    
    @RequestMapping("/getResult")
    public Object getResult(){
        return restTemplate.getForObject("http://localhost:8060/add?a={a}&b={b}",Integer.class,5,6);
    }
    
    @RequestMapping("/profile")
    public Object profile(){
        return restTemplate.getForObject("http://localhost:8040/spring-cloud-config-client/dev",String.class);
    }
    
    @RequestMapping("/profile/client")
    public Object client(){
        return restTemplate.getForObject("http://localhost:8041/profile",String.class);
    }
}
