package com.cloud.study.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class ServiceConsumerController {
    
    @Autowired
    private RestTemplate restTemplate;
    
    @RequestMapping("/add")
    public String add(Integer a,Integer b){
        String url="http://spring-cloud-service/add?a={a}&b={b}";
        if(a != null && b != null){
            url="http://spring-cloud-service/add?a="+a+"&b="+b;
        }
        return restTemplate.getForEntity(url, String.class,99,-18).getBody();
    }
}
